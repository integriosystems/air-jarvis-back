package com.air.jarvis.controller;

import com.air.jarvis.service.AirportTimeTableService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("airportTimeTable")
@RequiredArgsConstructor
public class AirportTimeTableController {
    private final AirportTimeTableService airportTimeTableService;

    @GetMapping("alternateFlights")
    public Object getAlternateFlights(@RequestParam String origin, @RequestParam String destination) {
        return airportTimeTableService.getAlternateFlights(origin, destination);
    }
}
