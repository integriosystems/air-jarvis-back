package com.air.jarvis.controller;

import com.air.jarvis.model.AirWaybill;
import com.air.jarvis.service.AirWaybillService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("airwaybills")
@RequiredArgsConstructor
public class AirWaybillController {
    private final AirWaybillService airWaybillService;

    @GetMapping("station/{station}")
    public List<AirWaybill> listAirWaybillsByStation(@PathVariable String station) {
        return airWaybillService.listAirWaybillsForStation(station);
    }

    @GetMapping("flight/{flightNumber}")
    public List<AirWaybill> listAirWaybillsByFlight(@PathVariable String flightNumber) {
        return airWaybillService.listAirWaybillsForFlight(flightNumber);
    }
}
