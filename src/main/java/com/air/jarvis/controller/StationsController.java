package com.air.jarvis.controller;

import com.air.jarvis.model.Station;
import com.air.jarvis.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("stations")
@RequiredArgsConstructor
public class StationsController {
    private final FlightService flightService;

    @GetMapping
    public List<Station> getStations(@RequestParam(required = false) String awbNumber) {
        return flightService.listStations(awbNumber);
    }
}
