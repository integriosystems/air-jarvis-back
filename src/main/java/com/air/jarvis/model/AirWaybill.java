package com.air.jarvis.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AirWaybill {
    private String awbNumber;
    private int pieces;
    private double weight;
    private AirWaybillStatus status;
    private List<String> routeStations;
    private List<String> handlingCodes;
}
