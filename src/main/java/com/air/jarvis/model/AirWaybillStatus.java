package com.air.jarvis.model;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public enum AirWaybillStatus {
    BOOKED("BKD"),
    MANIFESTED("MAN"),
    DEPARTED("DEP"),
    CANCELLED("CNL");

    private final String oneRecordCode;

    public static AirWaybillStatus getByOneRecordCode(String code) {
        for (AirWaybillStatus status : AirWaybillStatus.values()) {
            if (status.oneRecordCode.equals(code)) {
                return status;
            }
        }
        log.warn("AirWaybillStatus not mapped. OneRecord status code: " + code);
        return null;
    }
}
