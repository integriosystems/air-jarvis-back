package com.air.jarvis.model;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public enum FlightStatus {
    SCHEDULED("BKD", "MAN"),
    DEPARTED("DEP"),
    CANCELLED("CNL");

    private String[] oneRecordCodes;

    FlightStatus(String... oneRecordCodes) {
        this.oneRecordCodes = oneRecordCodes;
    }

    public static FlightStatus getByOneRecordCode(String code) {
        for (FlightStatus flightStatus : FlightStatus.values()) {
            for (String oneRecordCode : flightStatus.oneRecordCodes) {
                if (oneRecordCode.equals(code)) {
                    return flightStatus;
                }
            }
        }
        log.warn("FlightStatus not mapped. OneRecord status code: " + code);
        return null;
    }
}
