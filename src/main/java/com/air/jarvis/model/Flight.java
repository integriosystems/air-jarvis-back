package com.air.jarvis.model;

import com.air.jarvis.service.Weather;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
public class Flight {
    private String flightNumber;
    private LocalDateTime departureDateTime;
    private LocalDateTime arrivalDateTime;
    private FlightStatus status;
    private int secondsBehindSchedule;
    private String origin;
    private String destination;
    private double flightDurationMinutes;
    private double latitude;
    private double longitude;
    private ProbabilityOfProblem probabilityOfProblem;
    private long timeLeft;
    private Weather weatherAtOrigin;
    private Weather weatherAtDestination;
}
