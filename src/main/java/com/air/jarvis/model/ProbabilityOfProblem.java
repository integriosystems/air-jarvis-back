package com.air.jarvis.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProbabilityOfProblem {
    private double value;
    private ProbabilityOfProblemEvent event;

    private enum ProbabilityOfProblemEvent {
        DELAYED,
        CANCELLED;
    }
}
