package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class Geolocation implements Serializable {
    @JsonProperty("latitude")
    private Value latitude;
    @JsonProperty("longitude")
    private Value longitude;
    @JsonProperty("elevation")
    private Value elevation;
}
