package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class TransportSegment implements Serializable {
    @JsonProperty("departureLocation")
    private Location departureLocation;
    @JsonProperty("arrivalLocation")
    private Location arrivalLocation;
    @JsonProperty("bookingStatus")
    private BookingStatus bookingStatus;
    @JsonProperty("carriageStage")
    private String carriageStage;
    @JsonProperty("modeCode")
    private String modeCode;
    @JsonProperty("transportIdentifier")
    private String transportIdentifier;
    @JsonProperty("transportDate")
    private String transportDate;
    @JsonProperty("vehicleRegistration")
    private String vehicleRegistration;
    @JsonProperty("vehicleType")
    private String vehicleType;
    @JsonProperty("vehicleSize")
    private String vehicleSize;
    @JsonProperty("seal")
    private String seal;
    @JsonProperty("spaceAllocationDetails")
    private String spaceAllocationDetails;
    @JsonProperty("AllotmentDetails")
    private String allotmentDetails;
    @JsonProperty("exceptionHandlingDetails")
    private String exceptionHandlingDetails;
    @JsonProperty("deckPosition")
    private String deckPosition;

    @Getter
    @Setter
    @NoArgsConstructor
    public static class BookingStatus implements Serializable {
        @JsonProperty("code")
        private String code;
        @JsonProperty("description")
        private String description;
        @JsonProperty("dateTime")
        private String dateTime;
    }
}
