package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PieceGrouping implements Serializable {
    @JsonProperty("pieceDetails")
    private LogisticsComponent pieceDetails;
    @JsonProperty("ratingDetails")
    private Rating ratingDetails;
}
