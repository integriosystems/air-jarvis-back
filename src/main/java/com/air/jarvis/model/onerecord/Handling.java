package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class Handling implements Serializable {
    @JsonProperty("specialHandling")
    private List<SpecialHandling> specialHandling;
    @JsonProperty("serviceRequest")
    private List<ServiceRequest> serviceRequest;

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class SpecialHandling implements Serializable {
        @JsonProperty("code")
        private String code;
        @JsonProperty("description")
        private String description;
    }

    @Getter
    @Setter
    public static class ServiceRequest implements Serializable {
        @JsonProperty("code")
        private String code;
        @JsonProperty("description")
        private String description;
        @JsonProperty("statementType")
        private List<String> statementType;
        @JsonProperty("statementText")
        private String statementText;
    }
}
