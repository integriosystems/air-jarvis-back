package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TransportStatus {
    @JsonProperty("airWaybillNumber")
    private String airWaybillNumber;
    @JsonProperty("airWaybillReference")
    private String airWaybillReference;
    @JsonProperty("segmentStatus")
    private List<LogisticsComponent> segmentStatus;
}
