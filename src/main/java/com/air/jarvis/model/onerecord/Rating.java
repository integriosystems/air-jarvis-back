package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class Rating {
    @JsonProperty("rateClassCode")
    private String rateClassCode;
    @JsonProperty("commodityItemNumber")
    private String commodityItemNumber;
    @JsonProperty("ratingTypeIndicator")
    private String ratingTypeIndicator;
    @JsonProperty("rcp")
    private String rcp;
    @JsonProperty("loadTypeCode")
    private String loadTypeCode;
    @JsonProperty("uLDRateClassType")
    private String uldRateClassType;
    @JsonProperty("rateClassBasis")
    private String rateClassBasis;
    @JsonProperty("ratingClassPercentage")
    private String ratingClassPercentage;
    @JsonProperty("rateReferenceCode")
    private String rateReferenceCode;
    @JsonProperty("rateReferenceType")
    private String rateReferenceType;
    @JsonProperty("rateChargeAmount")
    private Value rateChargeAmount;
    @JsonProperty("subTotalAmount")
    private Value subTotalAmount;
}
