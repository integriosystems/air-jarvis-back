package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
public class Event implements Serializable {
    @JsonProperty("eventTypeIndicator")
    private String eventTypeIndicator;
    @JsonProperty("dateTime")
    private LocalDateTime dateTime;
    @JsonProperty("eventCode")
    private String eventCode;
    @JsonProperty("eventName")
    private String eventName;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("handlingDetails")
    private Handling handlingDetails;
}
