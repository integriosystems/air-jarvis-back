package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class Weight implements Serializable {
    @JsonProperty("netWeight")
    private Value netWeight;
    @JsonProperty("tareWeight")
    private Value tareWeight;
    @JsonProperty("grossWeight")
    private Value grossWeight;
}
