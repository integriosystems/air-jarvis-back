package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class Booking {
    @JsonProperty("bookingRequestNumber")
    private String bookingRequestNumber;
    @JsonProperty("date")
    private String date;
    @JsonProperty("airWaybillNumber")
    private String airWaybillNumber;
    @JsonProperty("airWaybillReference")
    private String airWaybillReference;
    @JsonProperty("bookingType")
    private String bookingType;
    @JsonProperty("segmentDetail")
    private List<TransportSegment> segmentDetail;
}
