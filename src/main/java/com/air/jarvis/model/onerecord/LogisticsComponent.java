package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class LogisticsComponent implements Serializable {
    @JsonProperty("goodsDescription")
    private String goodsDescription;
    @JsonProperty("weight")
    private Weight weight;
    @JsonProperty("containedPieceCount")
    private Integer containedPieceCount;
    @JsonProperty("originDestination")
    private TransportSegment originDestination;
    @JsonProperty("event")
    private List<Event> event;
    @JsonProperty("handlingDetails")
    private Handling handlingDetails;

    @Getter
    @Setter
    public static class PackagingType implements Serializable {
        @JsonProperty("code")
        private String code;
        @JsonProperty("description")
        private String description;
    }

    @Getter
    @Setter
    public static class ULD implements Serializable {
        @JsonProperty("type")
        private String type;
        @JsonProperty("aTAdesignator")
        private String ataDesignator;
        @JsonProperty("loadingIndicator")
        private String loadingIndicator;
        @JsonProperty("serialNumber")
        private String serialNumber;
        @JsonProperty("ownerCodeIssuer")
        private String ownerCodeIssuer;
        @JsonProperty("ownerCode")
        private String ownerCode;
    }

    public String getActualStatus() {
        return event.stream()
                .filter(event -> "Actual".equals(event.getEventTypeIndicator()))
                .map(Event::getEventCode)
                .findFirst()
                .orElse(null);
    }

    public LocalDateTime getDepartureDateTime() {
        return event.stream()
                .filter(event -> "Scheduled".equals(event.getEventTypeIndicator()))
                .filter(event -> "DEP".equals(event.getEventCode()))
                .map(Event::getDateTime)
                .findFirst()
                .orElse(null);
    }

    public LocalDateTime getArrivalDateTime() {
        return event.stream()
                .filter(event -> "Scheduled".equals(event.getEventTypeIndicator()))
                .filter(event -> "ARR".equals(event.getEventCode()))
                .map(Event::getDateTime)
                .findFirst()
                .orElse(null);
    }
}
