package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonContext implements Serializable {
    @JsonProperty("@vocab")
    private String vocab;
    @JsonProperty("@type")
    private String type;
}
