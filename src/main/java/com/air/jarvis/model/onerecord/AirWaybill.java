package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AirWaybill implements Serializable {
    @JsonProperty("@context")
    private JsonContext context;
    @JsonProperty("@type")
    private String type;
    @JsonProperty("AirWaybillNumber")
    private String airWaybillNumber;
    @JsonProperty("Date")
    private String date;
    @JsonProperty("LoadTypeCode")
    private String loadTypeCode;
    @JsonProperty("FlagConsolDirectLC")
    private String flagConsolDirectLC;
    @JsonProperty("AccountInformationCode")
    private String accountInformationCode;
    @JsonProperty("AccountInformationDescription")
    private String accountInformationDescription;
    @JsonProperty("PaymentMethodCode")
    private String paymentMethodCode;
    @JsonProperty("ServiceCode")
    private String serviceCode;
    @JsonProperty("AirlineProductIdentifier")
    private String airlineProductIdentifier;
    @JsonProperty("AirlineProductName")
    private String airlineProductName;
    @JsonProperty("OriginDestination")
    private TransportSegment originDestination;
    @JsonProperty("Shipper")
    private Company shipper;
    @JsonProperty("Consignee")
    private Company consignee;
    @JsonProperty("FreightForwarder")
    private Company freightForwarder;
    @JsonProperty("NotifyParty")
    private Company notifyParty;
    @JsonProperty("Carrier")
    private Company carrier;
    @JsonProperty("Routing")
    private Booking routing;
    @JsonProperty("WaybillLine")
    private List<PieceGrouping> waybillLine;
    @JsonProperty("Handling")
    private Handling handling;
    @JsonProperty("Insurance")
    private Insurance insurance;
    @JsonProperty("ChargeSummary")
    private ChargeSummary chargeSummary;
    @JsonProperty("TotalPieceCount")
    private int totalPieceCount;
    @JsonProperty("TotalSLAC")
    private String totalSLAC;
    @JsonProperty("TotalULDCount")
    private String totalULDCount;
    @JsonProperty("TotalNetWeight")
    private Weight totalNetWeight;
    @JsonProperty("TotalTareWeight")
    private Weight totalTareWeight;
    @JsonProperty("TotalGrossWeight")
    private Weight totalGrossWeight;
    @JsonProperty("TotalChargeableWeight")
    private Weight totalChargeableWeight;
    @JsonProperty("TotalVolume")
    private Value totalVolume;
    @JsonProperty("TotalChargeAmount")
    private Value totalChargeAmount;

    public static class AirWaybillList extends ArrayList<AirWaybill> {

    }
}
