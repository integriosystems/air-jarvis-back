package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
public class ChargeSummary {
    @JsonProperty("prepaid")
    private Prepaid prepaid;
    @JsonProperty("collect")
    private Collect collect;

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class Prepaid {
        @JsonProperty("totalWeightCharges")
        private Value totalWeightCharges;
        @JsonProperty("totalValuationCharges")
        private Value totalValuationCharges;
        @JsonProperty("totalTaxes")
        private Value totalTaxes;
        @JsonProperty("totalOtherChargesDueAgent")
        private Value totalOtherChargesDueAgent;
        @JsonProperty("totalOtherChargesDueCarrier")
        private Value totalOtherChargesDueCarrier;
        @JsonProperty("totalPrepaidCharges")
        private Value totalPrepaidCharges;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class Collect {
        @JsonProperty("totalWeightCharges")
        private Value totalWeightCharges;
        @JsonProperty("totalValuationCharges")
        private Value totalValuationCharges;
        @JsonProperty("totalTaxes")
        private Value totalTaxes;
        @JsonProperty("totalOtherChargesDueAgent")
        private Value totalOtherChargesDueAgent;
        @JsonProperty("totalOtherChargesDueCarrier")
        private Value totalOtherChargesDueCarrier;
        @JsonProperty("totalCollectCharges")
        private Value totalCollectCharges;
        @JsonProperty("inDestinationCurrency")
        private InDestinationCurrency inDestinationCurrency;

        @Getter
        @Setter
        public static class InDestinationCurrency {
            @JsonProperty("totalCharges")
            private Value totalCharges;
            @JsonProperty("collectionCharge")
            private Value collectionCharge;
            @JsonProperty("grandTotalCollectCharges")
            private Value grandTotalCollectCharges;
        }
    }
}
