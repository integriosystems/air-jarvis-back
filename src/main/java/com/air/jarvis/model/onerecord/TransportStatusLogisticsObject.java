package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransportStatusLogisticsObject {
    @JsonProperty("@context")
    private JsonContext context;
    @JsonProperty("@type")
    private String type;
    @JsonProperty("data")
    private TransportStatus data;
}
