package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
public class Insurance {
    @JsonProperty("coveringParty")
    private Company coveringParty;
    @JsonProperty("insuranceAmount")
    private Value insuranceAmount;
}
