package com.air.jarvis.model.onerecord;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class Company implements Serializable {
    @JsonProperty("name")
    private String name;
    @JsonProperty("iATACargoAgentCode")
    private String iataCargoAgentCode;
    @JsonProperty("branch")
    private List<Branch> branches;
    @JsonProperty("airlineCode")
    private String airlineCode;
    @JsonProperty("airlinePrefix")
    private String airlinePrefix;

    @Getter
    @Setter
    public static class Branch {
        @JsonProperty("branchName")
        private String name;
        @JsonProperty("iATACargoAgentLocationIdentifier")
        private String iataCargoAgentLocationIdentifier;
        @JsonProperty("otherIdentifier")
        private List<OtherIdentifier> otherIdentifiers;
        @JsonProperty("location")
        private Location location;

        @Getter
        @Setter
        public static class OtherIdentifier implements Serializable {
            @JsonProperty("identifierName")
            private String identifierName;
            @JsonProperty("identifer")
            private String identifer;
        }
    }
}
