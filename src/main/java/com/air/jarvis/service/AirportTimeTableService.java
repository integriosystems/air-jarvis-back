package com.air.jarvis.service;

import com.air.jarvis.service.aviationadge.AirportTimetable;
import com.air.jarvis.service.aviationadge.ScheduleItem;
import com.air.jarvis.utils.TimeZoneHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Service
public class AirportTimeTableService {
    @Value("${aviationedge.url}")
    private String timetablesUrl;
    @Value("${aviationedge.key}")
    private String aviationedgeKey;

    public List<ScheduleItem> getAlternateFlights(String origin, String destination) {
        RestTemplate restTemplate = new RestTemplate();

        UriComponentsBuilder uriComponentsBuilder = fromHttpUrl(timetablesUrl)
                .queryParam("iataCode", origin)
                .queryParam("type", "departure")
                .queryParam("airline_iata", "AC")
                .queryParam("key", aviationedgeKey);

        try {
            AirportTimetable airportTimetable = restTemplate.getForObject(uriComponentsBuilder.toUriString(), AirportTimetable.class);
            List<ScheduleItem> scheduleItems = new ArrayList<>();
            for (ScheduleItem scheduleItem : Objects.requireNonNull(airportTimetable)) {
                if ((scheduleItem.getStatus().equalsIgnoreCase("active") || scheduleItem.getStatus().equalsIgnoreCase("scheduled"))
                        && scheduleItem.getArrival().getIataCode().equalsIgnoreCase(destination)) {

                    ZoneId originTimeZone = TimeZoneHelper.getTimezone(origin);
                    if (originTimeZone != null) {
                        LocalDateTime scheduledTime = scheduleItem.getDeparture().getScheduledTime();
                        if (scheduledTime.isBefore(LocalDateTime.now())) {
                            scheduledTime = LocalDate.now().atTime(scheduledTime.toLocalTime());
                        }
                        ZonedDateTime scheduledZonedDateTime = scheduledTime.atZone(originTimeZone);
                        scheduleItem.setTimeLeft(Math.max(0, Duration.between(ZonedDateTime.now(), scheduledZonedDateTime).toMinutes()));
                    }

                    scheduleItems.add(scheduleItem);
                }
            }
            return scheduleItems;
        } catch (RestClientException e) {
            return Collections.emptyList();
        }
    }

}
