package com.air.jarvis.service.aviationadge;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleItem {
    private Airline airline;
    private Flight flight;
    private Departure departure;
    private Arrival arrival;
    private String status;
    private String type;
    private long timeLeft;
}
