package com.air.jarvis.service.aviationadge;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Arrival {
    private LocalDateTime actualRunway;
    private LocalDateTime actualTime;
    private String baggage;
    private String delay;
    private LocalDateTime estimatedRunway;
    private LocalDateTime estimatedTime;
    private String gate;
    private String iataCode;
    private String icaoCode;
    private LocalDateTime scheduledTime;
    private String terminal;
}
