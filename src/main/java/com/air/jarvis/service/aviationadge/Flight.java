package com.air.jarvis.service.aviationadge;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Flight{
    private String iataNumber;
    private String icaoNumber;
    private String number;
}
