package com.air.jarvis.service.aviationadge;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class AirportTimetable extends ArrayList<ScheduleItem> {
}
