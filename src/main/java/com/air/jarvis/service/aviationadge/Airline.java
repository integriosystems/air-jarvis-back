package com.air.jarvis.service.aviationadge;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Airline{
    private String iataCode;
    private String icaoCode;
    private String name;
}
