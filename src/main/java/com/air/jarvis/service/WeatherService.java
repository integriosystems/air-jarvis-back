package com.air.jarvis.service;

import com.air.jarvis.model.Coordinates;
import com.air.jarvis.service.openweathermap.OpenweathermapWeather;
import com.air.jarvis.utils.CoordinatesHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Service
public class WeatherService {
    @Value("${openweathermap.url}")
    private String weatherUrl;
    @Value("${openweathermap.key}")
    private String weatherKey;

    public Weather getWeather(String station) {
        Coordinates coordinates = CoordinatesHelper.getCoordinates(station);
        return getWeather(coordinates.getLat(), coordinates.getLon());
    }

    public Weather getWeather(double latitude, double longitude) {
        RestTemplate restTemplate = new RestTemplate();

        UriComponentsBuilder uriBuilder = fromHttpUrl(weatherUrl)
                .queryParam("appid", weatherKey)
                .queryParam("lat", latitude)
                .queryParam("lon", longitude);

        OpenweathermapWeather openweathermapWeather = Objects.requireNonNull(restTemplate.getForObject(uriBuilder.toUriString(), OpenweathermapWeather.class));

        Weather weather = new Weather();
        weather.setClouds(openweathermapWeather.getClouds().getAll());
        weather.setPressure(openweathermapWeather.getMain().getPressure());
        weather.setTemperature(openweathermapWeather.getMain().getTemp());
        weather.setVisibility(openweathermapWeather.getVisibility());
        weather.setWindSpeed(openweathermapWeather.getWind().getSpeed());
        if (openweathermapWeather.getWeather() != null) {
            weather.setIcon(openweathermapWeather.getWeather().stream()
                    .map(com.air.jarvis.service.openweathermap.Weather::getIcon)
                    .findFirst()
                    .orElse(null));
        }
        return weather;
    }

}
