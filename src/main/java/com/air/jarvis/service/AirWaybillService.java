package com.air.jarvis.service;

import com.air.jarvis.model.AirWaybill;
import com.air.jarvis.model.AirWaybillStatus;
import com.air.jarvis.model.FlightStatus;
import com.air.jarvis.model.onerecord.Handling;
import com.air.jarvis.model.onerecord.LogisticsComponent;
import com.air.jarvis.model.onerecord.TransportSegment;
import com.air.jarvis.model.onerecord.TransportStatus;
import com.air.jarvis.rest.OneRecordRestService;
import com.air.jarvis.rest.response.LogisticObjectResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.air.jarvis.model.onerecord.AirWaybill.AirWaybillList;

@Service
@RequiredArgsConstructor
public class AirWaybillService {
    private final OneRecordRestService oneRecordRestService;

    public List<AirWaybill> listAirWaybillsForStation(String station) {
        Map<String, AirWaybillStatus> airWaybillStatusMap = new HashMap<>();
        for (LogisticObjectResponse logisticObject : oneRecordRestService.listFlights()) {
            TransportStatus transportStatus = oneRecordRestService.getTransportStatus(logisticObject.getAwbNumber()).getData();
            for (LogisticsComponent logisticsComponent : transportStatus.getSegmentStatus()) {
                String departureLocation = logisticsComponent.getOriginDestination().getDepartureLocation().getCode();
                FlightStatus flightStatus = FlightStatus.getByOneRecordCode(logisticsComponent.getActualStatus());
                if (flightStatus == FlightStatus.SCHEDULED && departureLocation.equals(station)) {
                    AirWaybillStatus status = AirWaybillStatus.getByOneRecordCode(logisticsComponent.getActualStatus());
                    airWaybillStatusMap.put(logisticObject.getAwbNumber(), status);
                    break;
                }
            }
        }
        return listAirWaybills(airWaybillStatusMap);
    }

    public List<AirWaybill> listAirWaybillsForFlight(String flightNumber) {
        Map<String, AirWaybillStatus> airWaybillStatusMap = new HashMap<>();
        for (LogisticObjectResponse logisticObject : oneRecordRestService.listFlights()) {
            TransportStatus transportStatus = oneRecordRestService.getTransportStatus(logisticObject.getAwbNumber()).getData();
            for (LogisticsComponent logisticsComponent : transportStatus.getSegmentStatus()) {
                String transportIdentifier = logisticsComponent.getOriginDestination().getTransportIdentifier();
                if (transportIdentifier.equals(flightNumber)) {
                    AirWaybillStatus status = AirWaybillStatus.getByOneRecordCode(logisticsComponent.getActualStatus());
                    airWaybillStatusMap.put(logisticObject.getAwbNumber(), status);
                    break;
                }
            }
        }
        return listAirWaybills(airWaybillStatusMap);
    }

    private List<AirWaybill> listAirWaybills(Map<String, AirWaybillStatus> airWaybillStatusMap) {
        List<AirWaybill> airWaybills = new ArrayList<>();
        AirWaybillList list = oneRecordRestService.listAirWaybills();
        for (com.air.jarvis.model.onerecord.AirWaybill logisticObject : list) {
            if (!airWaybillStatusMap.containsKey(logisticObject.getAirWaybillNumber())) {
                continue;
            }
            AirWaybill airWaybill = new AirWaybill();
            airWaybill.setAwbNumber(logisticObject.getAirWaybillNumber());
            airWaybill.setStatus(airWaybillStatusMap.get(logisticObject.getAirWaybillNumber()));
            airWaybill.setPieces(logisticObject.getTotalPieceCount());
            airWaybill.setWeight(logisticObject.getTotalGrossWeight().getGrossWeight().getValue());
            airWaybill.setRouteStations(new ArrayList<>());
            List<TransportSegment> segmentDetails = logisticObject.getRouting().getSegmentDetail();
            for (int i = 0; i < segmentDetails.size(); i++) {
                TransportSegment transportSegment = segmentDetails.get(i);
                airWaybill.getRouteStations().add(transportSegment.getDepartureLocation().getCode());
                if (i == segmentDetails.size() - 1) {
                    airWaybill.getRouteStations().add(transportSegment.getArrivalLocation().getCode());
                }
            }
            airWaybill.setHandlingCodes(new ArrayList<>());
            if (logisticObject.getHandling() != null) {
                for (Handling.SpecialHandling specialHandling: logisticObject.getHandling().getSpecialHandling()) {
                    airWaybill.getHandlingCodes().add(specialHandling.getCode());
                }
            }
            airWaybills.add(airWaybill);
        }
        return airWaybills;
    }
}
