package com.air.jarvis.service;

import com.air.jarvis.model.*;
import com.air.jarvis.model.onerecord.Location;
import com.air.jarvis.model.onerecord.LogisticsComponent;
import com.air.jarvis.model.onerecord.TransportSegment;
import com.air.jarvis.model.onerecord.TransportStatus;
import com.air.jarvis.rest.OneRecordRestService;
import com.air.jarvis.rest.response.LogisticObjectResponse;
import com.air.jarvis.utils.TimeZoneHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FlightService {
    private final WeatherService weatherService;
    private final OneRecordRestService oneRecordRestService;

    public List<Flight> listFlights(String awbNumber) {
        List<Flight> flights = new ArrayList<>();
        for (LogisticObjectResponse logisticObject : oneRecordRestService.listFlights()) {
            if (awbNumber != null && !awbNumber.equals(logisticObject.getAwbNumber())) {
                continue;
            }
            TransportStatus transportStatus = oneRecordRestService.getTransportStatus(logisticObject.getAwbNumber()).getData();
            for (LogisticsComponent logisticsComponent : transportStatus.getSegmentStatus()) {
                TransportSegment originDestination = logisticsComponent.getOriginDestination();
                Location departureLocation = originDestination.getDepartureLocation();
                Location arrivalLocation = originDestination.getArrivalLocation();

                String flightNumber = originDestination.getTransportIdentifier();
                if (flights.stream().anyMatch(flight -> flight.getFlightNumber().equals(flightNumber))) {
                    continue;
                }

                String status = logisticsComponent.getActualStatus();
                LocalDateTime departureDateTime = logisticsComponent.getDepartureDateTime();
                LocalDateTime arrivalDateTime = logisticsComponent.getArrivalDateTime();

                Flight flight = new Flight();
                flight.setFlightNumber(flightNumber);
                flight.setOrigin(departureLocation.getCode());
                flight.setDestination(arrivalLocation.getCode());
                flight.setDepartureDateTime(departureDateTime);
                flight.setArrivalDateTime(arrivalDateTime);
                flight.setStatus(FlightStatus.getByOneRecordCode(status));
                flight.setSecondsBehindSchedule(0); // TODO
                flight.setFlightDurationMinutes(0); // TODO

                ZoneId departureTimeZone = TimeZoneHelper.getTimezone(departureLocation.getCode());
                ZonedDateTime zdt = departureDateTime.atZone(departureTimeZone);
                flight.setTimeLeft(Math.max(0, Duration.between(ZonedDateTime.now(), zdt).toMinutes()));

                Coordinates coordinates = oneRecordRestService.getCoordinates(flightNumber);
                flight.setLatitude(coordinates.getLat());
                flight.setLongitude(coordinates.getLon());

                ProbabilityOfProblem probabilityOfProblem = oneRecordRestService.getFlightProbability(flightNumber, departureDateTime);
                flight.setProbabilityOfProblem(probabilityOfProblem);

                flight.setWeatherAtOrigin(weatherService.getWeather(departureLocation.getCode()));
                flight.setWeatherAtDestination(weatherService.getWeather(arrivalLocation.getCode()));

                flights.add(flight);
            }
        }
        return flights;
    }


    public List<Station> listStations(String awbNumber) {
        List<Station> stations = new ArrayList<>();
        for (LogisticObjectResponse logisticObject : oneRecordRestService.listFlights()) {
            if (awbNumber != null && !awbNumber.equals(logisticObject.getAwbNumber())) {
                continue;
            }
            TransportStatus transportStatus = oneRecordRestService.getTransportStatus(logisticObject.getAwbNumber()).getData();
            for (LogisticsComponent logisticsComponent : transportStatus.getSegmentStatus()) {
                TransportSegment originDestination = logisticsComponent.getOriginDestination();
                putStationLocation(stations, originDestination.getDepartureLocation());
                Location arrivalLocation = originDestination.getArrivalLocation();
                putStationLocation(stations, arrivalLocation);
            }
        }
        return stations;
    }

    private void putStationLocation(List<Station> stations, Location location) {
        if (stations.stream().noneMatch(station -> station.getName().equals(location.getCode()))) {
            double lat = location.getGeoLocation().getLatitude().getValue();
            double lon = location.getGeoLocation().getLongitude().getValue();
            stations.add(new Station()
                    .setName(location.getCode())
                    .setLatitude(lat)
                    .setLongitude(lon));
        }
    }
}
