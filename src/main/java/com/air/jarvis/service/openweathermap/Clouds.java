package com.air.jarvis.service.openweathermap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Clouds {
    private int all;
}
