package com.air.jarvis.service;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
public class FlightProbability {

    private final static double DEFAULT_DELAY_PROBABILITY = 0.03;
    private final static double DEFAULT_CANCEL_PROBABILITY = 0.01;


    private Weather weatherAtOrigin;
    private Weather weatherAtDestination;
    private LocalDateTime departureDate;
    private LocalDateTime arrivalDate;

    @Getter
    private double delayProbability;
    @Getter
    private double cancelProbability;


    private FlightProbability() {
    }

    public static FlightProbability getInstance(
            Weather weatherAtOrigin,
            Weather weatherAtDestination,
            LocalDateTime departureDate,
            LocalDateTime arrivalDate
    ) {
        FlightProbability ret = new FlightProbability();
        ret.weatherAtOrigin = weatherAtOrigin;
        ret.weatherAtDestination = weatherAtDestination;
        ret.departureDate = departureDate;
        ret.arrivalDate = arrivalDate;
        ret.calculateProbabilities();
        return ret;
    }

    private void calculateProbabilities() {
        if (LocalDateTime.now().compareTo(departureDate) < 0) {
            return;
        }
        double destinationWeatherConditions = weatherAtDestination.getWeatherConditions();
        double originWeatherConditions = weatherAtOrigin.getWeatherConditions();
        delayProbability = Math.max(destinationWeatherConditions + DEFAULT_DELAY_PROBABILITY, originWeatherConditions);
        cancelProbability = delayProbability / 3.5F;
    }

}
