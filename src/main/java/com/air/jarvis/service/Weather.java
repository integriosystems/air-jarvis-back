package com.air.jarvis.service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Weather {
    private double clouds;
    private double temperature;
    private double windSpeed;
    private double visibility;
    private double probabilityOfPrecipitation;
    private int pressure;
    private String icon;

    //from 0 to 1. 0 is good 1 is crazy
    //very simplified
    public double getWeatherConditions(){
        double ret = 0;
        if(clouds > 80) {
            ret += 0.05;
        }
        if(windSpeed > 50) {
            ret += 0.8;
        } else if (windSpeed > 30) {
            ret += 0.6;
        } else if (windSpeed > 20) {
            ret += 0.2;
        }
        if(visibility < 200) {
            ret += 0.2;
        }
        return Math.min(1, ret);
    }
}
