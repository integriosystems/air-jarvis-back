package com.air.jarvis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirJarvisApplication {

    public static void main(String[] args) {
        if (System.getProperty("spring.config.name") == null) {
            System.setProperty("spring.config.name", "settings");
        }
        SpringApplication.run(AirJarvisApplication.class, args);
    }
}
