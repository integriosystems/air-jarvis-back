package com.air.jarvis.utils;

import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class TimeZoneHelper {
    private static Map<String, ZoneId> stationTimezones = new HashMap<>();

    static {
        stationTimezones.put("YVR", ZoneId.of("America/Vancouver"));
        stationTimezones.put("YYZ", ZoneId.of("America/Toronto"));
        stationTimezones.put("YYC", ZoneId.of("MST7MDT"));
        stationTimezones.put("YUL", ZoneId.of("America/Montreal"));
        stationTimezones.put("YHZ", ZoneId.of("Canada/Eastern"));
        stationTimezones.put("LHR", ZoneId.of("Europe/London"));
        stationTimezones.put("ORD", ZoneId.of("America/Chicago"));
        stationTimezones.put("YOW", ZoneId.of("Canada/Eastern"));
        stationTimezones.put("PVG", ZoneId.of("Asia/Shanghai"));
        stationTimezones.put("HKG", ZoneId.of("Hongkong"));
        stationTimezones.put("FRA", ZoneId.of("Europe/Berlin"));
        stationTimezones.put("YYJ", ZoneId.of("Canada/Pacific"));
        stationTimezones.put("YYG", ZoneId.of("Canada/Atlantic"));
    }

    public static ZoneId getTimezone(String station) {
        return stationTimezones.get(station);
    }
}
