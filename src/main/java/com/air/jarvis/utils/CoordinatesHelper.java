package com.air.jarvis.utils;

import com.air.jarvis.model.Coordinates;

import java.util.HashMap;
import java.util.Map;

public class CoordinatesHelper {
    private static final Map<String, Coordinates> stationCoordinates = new HashMap<>();

    static {
        stationCoordinates.put("YVR", new Coordinates(49.1902, -123.1837));
        stationCoordinates.put("YYZ", new Coordinates(43.651070, -79.347015));
        stationCoordinates.put("YYC", new Coordinates(51.131470, -114.010559));
    }

    public static Coordinates getCoordinates(String station) {
        return stationCoordinates.get(station);
    }
}
