package com.air.jarvis.rest.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class LogisticObjectResponse {
    private String id;
    private String type;
    private String awbNumber;

    @Getter
    @Setter
    public static class LogisticObjectResponseList extends ArrayList<LogisticObjectResponse> {
    }
}
