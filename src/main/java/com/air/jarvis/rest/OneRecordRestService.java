package com.air.jarvis.rest;

import com.air.jarvis.model.ProbabilityOfProblem;
import com.air.jarvis.model.onerecord.AirWaybill;
import com.air.jarvis.model.onerecord.TransportStatusLogisticsObject;
import com.air.jarvis.model.Coordinates;
import com.air.jarvis.rest.response.LogisticObjectResponse.LogisticObjectResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
@RequiredArgsConstructor
public class OneRecordRestService {
    @Value("${one-record.url}")
    private String oneRecordUrl;

    private final RestTemplate oneRecordRestTemplate;

    public AirWaybill.AirWaybillList listAirWaybills() {
        UriComponentsBuilder uriBuilder = fromHttpUrl(oneRecordUrl + "logisticObject/list")
                .queryParam("type", "Airwaybill");
        return oneRecordRestTemplate.getForObject(uriBuilder.toUriString(), AirWaybill.AirWaybillList.class);
    }

    public LogisticObjectResponseList listFlights() {
        UriComponentsBuilder uriBuilder = fromHttpUrl(oneRecordUrl + "logisticObject/list")
                .queryParam("type", "TransportStatus");
        return oneRecordRestTemplate.getForObject(uriBuilder.toUriString(), LogisticObjectResponseList.class);
    }

    public TransportStatusLogisticsObject getTransportStatus(String awbNumber) {
        UriComponentsBuilder uriComponentsBuilder = fromHttpUrl(oneRecordUrl + "logisticObject/getCurrentTransportStatus")
                .queryParam("awbNumber", awbNumber);
        return oneRecordRestTemplate.getForObject(uriComponentsBuilder.toUriString(), TransportStatusLogisticsObject.class);
    }

    public Coordinates getCoordinates(String flightNumber) {
        UriComponentsBuilder uriComponentsBuilder = fromHttpUrl(oneRecordUrl + "logisticObject/getCoordinates")
                .queryParam("flightNumber", flightNumber);
        return oneRecordRestTemplate.getForObject(uriComponentsBuilder.toUriString(), Coordinates.class);
    }

    public ProbabilityOfProblem getFlightProbability(String flightNumber, LocalDateTime dateTime) {
        UriComponentsBuilder uriComponentsBuilder = fromHttpUrl(oneRecordUrl + "logisticObject/getFlightProbability")
                .queryParam("flightNumber", flightNumber)
                .queryParam("dateTime", dateTime);
        return oneRecordRestTemplate.getForObject(uriComponentsBuilder.toUriString(), ProbabilityOfProblem.class);
    }

}
